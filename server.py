import flwr as fl
from flwr.server.strategy import FedAvg

# Start Flower server for three rounds of federated learning
if __name__ == "__main__":
    # FedAvg is the default strategy used when you start the server without a custom strategy
    strategy = FedAvg(
        # Minimum number of connected clients before sampling e.g. 10
        min_available_clients=10,

        # Fraction of clients which should participate in each round
        fraction_fit=0.3
    )
    fl.server.start_server("127.0.0.1:8080", strategy=strategy, config={"num_rounds": 3})