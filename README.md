# Flower Experiment

In this example a DL model is trained on CIFAR dataset using the TensorFlow library in a federated manner with Flower framework. In particular, 10 clients collaboratively train a DL model for image classification.

#Run the example

-Install dependencies from requirements.txt

-Clone the repo

-First execute the Server.py and then execute at least 10 clients
